function sprite(sizeX, sizeY, row, speed, frames, border) {
	this.sizeX = sizeX;
	this.sizeY = sizeY;
	this.speed = speed;
	this.frames = frames;
	this.border = border;
	this.backToDefault = false;
	this.row = row;

	this.x = 0;
	this.y = 0;
	var frame = 0;
	var frameNumber = 0;
	var duration = 10;
	var n = 0;
	var length = this.frames.length - 1;

	this.update = function() {
		if(this.frames.length > 1) {
			n++;
			if(n % this.speed == 0) {
				if(frame < length){
					frame++;
				}
				else {
					frame = 0;
					return;
				}
			}
		}
		else {
			frame = 0;
		}
		frameNumber = this.frames[frame] - 1;

		this.x = frameNumber * this.sizeY + this.border.x;
		this.y = this.row * this.sizeY + this.border.y;
	}
}
