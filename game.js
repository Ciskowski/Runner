document.body.insertBefore(canvas, document.body.childNodes[0]);
const STATIC_START_N = 120;

var obstacles = [];
var frameNo = 0;
var n = STATIC_START_N;
var minRand = 42;
var maxRand = 50;
var gameFinished = false;
var generateItems = true;
var gameTimer = null;
var startCountingPoints = false;

var keys = [];
var keyMap = {
	Jump: 38,
	Fall: 40,
}

var playerParams = {
	width: 70,
	height: 70,
	x: 50,
	y: floorHeight,
}

function start(){
	gameFinished = false;
	player = new player(playerParams.width, playerParams.height,
							 0 - playerParams.width * 1.5, playerParams.y);

    window.addEventListener('keydown',function(e){
       keys[e.keyCode] = true;
    });

    window.addEventListener('keyup',function(e){
    	delete keys[e.keyCode];
    });

	gameStart();
}

function gameStart() {
	obstacleSpeed = STATIC_OBSTACLE_SPEED;
	gameTimer = setInterval(gameLoop, 1000/60);
	frameNo = 0;
	points = 0;
	n = STATIC_START_N;
	player.resetJump();
	player.x = 0 - playerParams.width * 1.5;
	obstacles = [];
	maxRand = 50;
	minRand = 42;
}

function gameLoop(){
	update();
	draw();
	controller();
}

function controller(){
	if(keys && keys[keyMap.Jump] && !gameFinished){
		player.jump();
	} else if(keys && keys[keyMap.Fall]){
		player.fall();
	}
}

function update(){
	frameNo++;
	startAnimation();
	obstacles.forEach((item, index) => {
		if(player.isCollide(item) && !gameFinished)
			gameOver();
		item.move();
	});
	player.update();
	player.updateJumping();
	level();
}

function draw(){
	context.clearRect(0, 0, canvas.width, canvas.height);

	if(startCountingPoints)
		points += 0.1;
	context.font = "30px Arial";
	context.fillText(`${Math.floor(points)} pkt`, 10,30);

	removeObstacle(obstacles);
	if(everyInteval()){
		obstacles.push(new obstacle(obstacleTypes[randomizeObstacle()]));
		frameNo = 0;
		n = Math.floor((Math.random() * maxRand) + minRand);
	};
	obstacles.forEach((item, index) => {
		drawObject(item);
	});

	drawObject(player);
}

function drawObject(obj){
	if(obj.image == null) {
		context.fillStyle = "red";
		context.fillRect(obj.x, obj.y, obj.width, obj.height);
	} else {
		if(obj.hasSprite) {
			context.drawImage(obj.image, obj.spriteX, obj.spriteY, obj.width, obj.height,
								 obj.x, obj.y, obj.width, obj.height);
		} else {
			context.drawImage(obj.image, obj.x, obj.y, obj.width, obj.height);
		}
	}
}

function level() {
	let levelPoints = Math.floor(points);

	if(levelPoints > 1 && !gameFinished) {
		if(minRand + maxRand > 5) {
			minRand -= 0.005;
			maxRand -= 0.0006;
			obstacleSpeed += 0.004;
		}
	}
}

function startAnimation() {
		if(player.x < playerParams.x) {
			player.x ++;
		}
}

function everyInteval() {
	if ((frameNo / n) % 1 == 0 && generateItems) {
		if(!gameFinished)
			startCountingPoints = true;

		return true;
	}

	return false;
}

function removeObstacle(array) {
	array.forEach((item, index) => {
		if(item.x + item.width < 0)
			array.shift();
	});
}

function randomizeObstacle() {
	let arrayLength = obstacleTypes.length;
	return Math.floor(Math.random() * arrayLength)
}

function gameOver() {
	gameFinished = true;
	var pause = null;
	keys = [];
	let breakPoint = 0;
	player.hit();
	var deadDelay = player.hitDelay();
	startCountingPoints = false;

	function waitForKey() {
		breakPoint++;
		if(obstacleSpeed > 0.7) {
			obstacleSpeed -= 0.6;
		} else {
			obstacleSpeed = 0;
			generateItems = false;
		}
		player.fall();
		if(breakPoint == deadDelay) {
			player.dead();
		}
		if(obstacleSpeed <= 0 && keys.length > 0){
			clearInterval(gameTimer);
			clearInterval(pause);
			gameStart();
			gameFinished = false;
			generateItems = true;
			player.reset();
		}
	}

	pause = setInterval(waitForKey, 1000 / 30);
}

start();
