const STATIC_JUMP_HEIGHT = 220;
const STATIC_JUMP_SPEED = 0.65;
const STATIC_JUMP_FALL = 0.9;

var jumpHeight = STATIC_JUMP_HEIGHT;
var jumpSpeed = STATIC_JUMP_SPEED;

const spriteBorder = { x: 10, y: 10}

function player(width, height, x, y){
	this.image = new Image();
	this.image.src = 'img/player.jpg';
	this.width = width;
	this.height = height;
	this.x = x;
	this.y = y;
	this.spriteX = 0;
	this.spriteY = 0;
	this.hasSprite = true;
	this.isHit = false;

	var defaultSprite = new sprite(85, 85, 1, 10, [3,4,5,6], {x:5, y:15});
	playerSprite = defaultSprite;
	playerSprite.backToDefault = false;

	this.update = function() {
		if(resetToDefault() && !this.isHit)
			playerSprite = defaultSprite;

		this.spriteX = playerSprite.x;
		this.spriteY = playerSprite.y;
		playerSprite.update();
	}

	resetToDefault = function() {
		if(playerSprite.backToDefault)
			return true;
		return false;
	}

	this.isJumping = false;
	this.jump = function(){
		this.isJumping = true;
	}

	this.fall = function(){
		if(this.isJumping && axisX > 0 && !this.isHit) {
			jumpSpeed = STATIC_JUMP_FALL;
		}
	}

	var quadraticX = Math.sqrt(-4*-1*STATIC_JUMP_HEIGHT)/ (2*-1);
	let axisX = quadraticX;

	this.updateJumping = function() {
		if(this.isJumping){
			axisX += jumpSpeed;
			this.y = floorHeight - (-1* Math.pow(axisX,2)+jumpHeight);

			if(!this.isHit){
				if(axisX > 0) {
					playerSprite = new sprite(85, 85, 1, null, [1], {x:5, y:15});
				} else {
					playerSprite = new sprite(85, 85, 1, null, [2], {x:5, y:15});
				}
				playerSprite.update();
			}

			if(this.y >= floorHeight){
				this.isJumping = false;
				this.y = floorHeight;
				axisX = quadraticX;
				jumpHeight = STATIC_JUMP_HEIGHT;
				jumpSpeed = STATIC_JUMP_SPEED;
				playerSprite.backToDefault =  true;
			}
		}
	}

	this.isCollide = function(obstacle) {
		if(this.x + this.width >= obstacle.x && this.x <= obstacle.x + obstacle.width
			&&  this.y + this.height >= obstacle.y && this.y <= obstacle.y + obstacle.height){
				//this.isJumping = false;
			return true;
		}

		return false;
	}

	var isHitAnimation = false;
	this.hit = function() {
		this.isHit = true;
		playerSprite = new sprite(85, 85, 4, 10, [7,6,5], {x:5, y:15});
	}
	this.hitDelay = function() {
		return playerSprite.frames.length * playerSprite.speed / 2.5;
	}

	this.dead = function() {
		playerSprite = new sprite(85, 85, 4, 10, [4,3,2,1,5], {x:5, y:15});
		playerSprite.update();
	}

	this.resetJump = function() {
		axisX = quadraticX;
		this.isJumping = false;
		this.y = floorHeight;
		jumpSpeed = STATIC_JUMP_SPEED;
	}

	this.reset = function() {
		playerSprite.backToDefault = true;
		this.isHit = false;
	}
}
