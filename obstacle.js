var STATIC_OBSTACLE_SPEED = 8
var obstacleSpeed = STATIC_OBSTACLE_SPEED;

function obstacleParams(type, width, height){
	this.width = width;
	this.height = height;
	this.type = type;
	switch(type){
		case 'rock1':
			this.img = "img/rock.png";
			break;
		case 'tree1':
			this.img = "img/tree.png";
			break;
		case 'rock2':
			this.img = 'img/bigrock.png';
			break;
		default:
			this.img = 'none.png';
			break;
	}
}

var obstacleTypes = [
	new obstacleParams("rock1", 50, 50),
	new obstacleParams("tree1", 50, 100),
	new obstacleParams("rock2", 75, 75),
]

function obstacle(obstacleType, y) {
	this.image = new Image();
	this.image.src = obstacleType.img;
	this.width = obstacleType.width;
	this.height = obstacleType.height;
	this.x = canvas.width + this.width;
	this.horizon = -this.height + 70; //is relating to player height
	this.y = playerParams.y + this.horizon;

	this.move = function() {
		this.x -= obstacleSpeed;
	}
}
